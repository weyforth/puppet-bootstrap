#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -n "Master FQDN: "
read FACTER_masterfqdn

echo -n "Agent FQDN: "
read FACTER_agentfqdn

echo -n "Environment: "
read FACTER_agentenvironment

puppet module install Aethylred/puppet --version 1.5.3
git clone https://weyforth@bitbucket.org/weyforth/puppet-agent.git /etc/puppet/modules/agent

FACTER_agentfqdn=$FACTER_agentfqdn FACTER_agentenvironment=$FACTER_agentenvironment FACTER_masterfqdn=$FACTER_masterfqdn puppet apply -e 'include agent'

rm -rf /etc/puppet/modules/*/