#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

defaultmasterfqdn="puppet.example.com"

echo -n "Master FQDN: [$defaultmasterfqdn] "
read FACTER_masterfqdn

defaultr10krepo="https://weyforth@bitbucket.org/weyforth/puppet-repository.git"

echo -n "r10k repo: [$defaultr10krepo] "
read FACTER_r10krepo

[[ $FACTER_r10krepo == "" ]] && FACTER_r10krepo="$defaultr10krepo"
[[ $FACTER_masterfqdn == "" ]] && FACTER_masterfqdn="$defaultmasterfqdn"

apt-get install -y ruby-dev make
gem install librarian-puppet -v 2.1.0

temp_dir="$(mktemp -d)"

pushd "${temp_dir}"
	git clone $FACTER_r10krepo .
	git checkout puppet_master
	cp Puppetfile /etc/puppet/
popd

pushd /etc/puppet/
	librarian-puppet install --verbose
popd


FACTER_masterfqdn=$FACTER_masterfqdn FACTER_r10krepo=$FACTER_r10krepo puppet apply "${temp_dir}/manifests/site.pp"

rm -rf "${temp_dir}"
rm -f /etc/puppet/Puppetfile
rm -rf /etc/puppet/modules/*/