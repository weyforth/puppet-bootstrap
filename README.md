# Puppet Bootstrap

Start up a server, and run:
(Change the last script to your particular operating system)

```bash
apt-get install -y git
git clone https://weyforth@bitbucket.org/weyforth/puppet-bootstrap.git
cd puppet-bootstrap
./scripts/init/ubuntu.sh
```

## Master

```bash
./scripts/bootstrap/master.sh
```

## Agent

```bash
./scripts/bootstrap/agent.sh
```
